{-# LANGUAGE RecordWildCards #-}

module User.Storage (Handle(..), new) where

import Control.Monad.STM
import StmContainers.Map.Extended (Map)
import qualified StmContainers.Map.Extended as Map

import User

data Handle = Handle
    {  put :: User -> IO Int
     , getAll :: IO [User]
    }

new :: IO Handle
new = do
  userDb <- Map.newIO
  pure Handle
    { put = _put userDb
    , getAll = _getAll userDb
    }

type UserDb = Map Int User

_getAll :: UserDb ->  IO [User]
_getAll = atomically . Map.values

_put :: UserDb -> User -> IO Int
_put userDb user = atomically $ do
  newKey <- nextKey userDb
  Map.insert user newKey userDb
  pure newKey

nextKey :: UserDb -> STM Int
nextKey userDb = do
  keys <- Map.keys userDb
  pure $ if null keys
         then 0
         else maximum keys + 1

close :: IO ()
close = undefined