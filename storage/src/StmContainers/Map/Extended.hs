module StmContainers.Map.Extended
    ( module Map
    , keys
    , values
    )
  where

import Control.Monad.STM
import StmContainers.Map as Map
import ListT (toList)

keys :: Map k v -> STM [k]
keys map = do
  recs <- toList $ Map.listT map
  pure $ fst <$> recs

values :: Map k v -> STM [v]
values map = do
  recs <- toList $ Map.listT map
  pure $ snd <$> recs
