module API (API, server, writeSwaggerJSON) where

import Data.Aeson.Encode.Pretty (encodePretty)
import qualified Data.ByteString.Lazy.Char8 as BL8
import Servant
import Servant.Swagger.UI
import User
import User.API
import User.Storage

type SwaggerAPI =  SwaggerSchemaUI "swagger" "swagger.json"

type API = SwaggerAPI :<|> UserAPI

writeSwaggerJSON :: IO ()
writeSwaggerJSON = BL8.writeFile "swagger.json" (encodePretty userSwagger)

server :: User.Storage.Handle -> Server API
server handle = swaggerSchemaUIServer userSwagger :<|> (userServer handle)
