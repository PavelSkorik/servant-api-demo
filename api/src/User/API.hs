module User.API where

import User.Storage
import User
import Data.Proxy
import Control.Lens
import Data.Aeson
import Data.Swagger
import Servant
import Servant.Swagger
import Servant.Swagger.UI
import Control.Monad.IO.Class

type UsersGet = "user" :> Get '[JSON] [User]
type UserPost = "user" :> ReqBody '[JSON] User :> Post '[JSON] Int
type UserAPI = UsersGet :<|> UserPost

instance ToSchema User where
  declareNamedSchema proxy = genericDeclareNamedSchema defaultSchemaOptions proxy
    & mapped.schema.description ?~ "User"
    & mapped.schema.example ?~ toJSON (User "Pasha" 27)

userAPI :: Proxy UserAPI
userAPI = Proxy

userSwagger :: Swagger
userSwagger = toSwagger userAPI
  & info.title   .~ "User API"
  & info.version .~ "1.0"
  & info.description ?~ "This is an API that tests swagger integration"
  & info.license ?~ ("MIT" & url ?~ URL "http://mit.com")


userServer :: User.Storage.Handle -> Server UserAPI
userServer User.Storage.Handle{..} = get :<|> post
  where 
        get :: Handler [User]
        get = liftIO getAll

        post :: User -> Handler Int
        post user = liftIO $ put user