{-# LANGUAGE DeriveGeneric #-}

module User (User(..)) where

import GHC.Generics
import Data.Aeson

data User = User 
    { name :: String
    , age :: Int
    } deriving (Generic, Show)
    
instance ToJSON User
instance FromJSON User