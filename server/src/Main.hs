module Main (main) where

import Servant
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.Cors
import User.Storage
import API

main :: IO ()
main = do
    userStorage <- User.Storage.new
    Warp.run 8080 (simpleCors $ serve (Proxy @API) $ server userStorage)